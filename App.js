import { StatusBar } from 'expo-status-bar';
import { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity, ScrollView } from 'react-native';

export default function App() {
  const[count, setCount] = useState(0);
  const[dado, setDado] =useState(0);

  function rodarDado(){
    setDado(Math.floor(Math.random() * 6) + 1);
  }
  return (
    
    <View style={styles.container}>
      <ScrollView>
      {/* 
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      <Text>7777777777777777777777777777</Text>
      */}
      <Text>Clique para Contar</Text> 
      <TouchableOpacity style={(styles.teste)}
        onPress={() => setCount(count + 1)}>
          <Text> CLICAR </Text>
        </TouchableOpacity>

        <TouchableOpacity
        style={styles.teste}
        onPress={() => rodarDado()}
        >
          <Text>Este Código Está Funcionando</Text>
        </TouchableOpacity>
        <Text> Número Sorteado {dado} </Text>
      </ScrollView>
      {/* <StatusBar style="auto" /> */}
    </View>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  teste: {
    borderRadius:25,
    backgroundColor: 'red',
    alignItems: 'center',
    color:"black"
    
  },
});
